#!/usr/bin/env bash

set -euo pipefail

# Emulate `cp -a` (preserve symlinks, etc) for one folder to another.
function cp_a() {
    if [[ $# -ne 2 ]]; then
        echo "Usage: ${FUNCNAME[0]} source destination"
    fi
    local source="$1"; shift
    local destination="$1"; shift

    mkdir -p "$destination"
    rsync -cav "$source/" "$destination/"
}

# Download and install the browser
function download_browser() {
    if [[ $# -ne 2 ]]; then
        echo "Usage: ${FUNCNAME[0]} browser_name install_folder"
    fi
    local browser_name="$1"; shift
    local browser_app="$1"; shift

    echo "Missing browser"
    local tmp="${browser_app%/*}"
    mkdir -p "$tmp/mount"

    case "$browser_name" in
        'Google Chrome')
            url_dmg='https://dl.google.com/chrome/mac/stable/GGRO/googlechrome.dmg'
        ;;

        'Brave Browser')
            url_dmg='https://referrals.brave.com/latest/Brave-Browser.dmg'
        ;;

        'Firefox')
            url_dmg='https://download.mozilla.org/?product=firefox-latest-ssl&os=osx&lang=en-US'
        ;;
    esac

    [[ -e "$tmp/browser.dmg" ]] || curl --location --output "$tmp/browser.dmg" "$url_dmg"
    yes | hdiutil attach -noverify -nobrowse -mountpoint "$tmp/mount" "$tmp/browser.dmg" || true
    cp_a "$tmp/mount/"*.app/ "$browser_app/"
    hdiutil detach "$tmp/mount"
    rm -rf "$tmp/mount" "$tmp/browser.dmg"
}

# Start the browser
function browser_exec() {
    if [[ $# -lt 2 ]]; then
        echo "Usage: ${FUNCNAME[0]} browswe_app data_folder"
    fi
    local browser_app="$1"; shift
    local data_folder="$1"; shift

    # Find the browser's executable name
    local browser_exec=$(/usr/libexec/PlistBuddy -c 'print :CFBundleExecutable' "$browser_app/Contents/Info.plist")

    # Build the browser's command
    local command=(
        "$browser_app/Contents/MacOS/$browser_exec"
    )

    case "$browser_exec" in
        'Google Chrome' | 'Brave Browser')
            command+=(
                --new-window
                --user-data-dir="$data_folder"
            )
        ;;

        'firefox')
            command+=(
                --profile "$data_folder"
            )
        ;;
    esac

    # Start the browser
    exec "${command[@]}" "$@"
}

# Get the app's folder. This is easy as the app is supposed to be:
#
# Foo.app
# └── Contents
#    ├── Info.plist
#    ├── MacOS
#    │   └── browser.sh <-- this script
#    └── Resources
#        └── app.icns
#
# Thus we need to go 2 folders up from the current script and get the folder name
app_macos=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
app_folder=$(cd "${app_macos}/../../" && pwd)
app_name=$(basename -s .app "$app_folder")

# Ensure that we have the browser embedded
browser_app="$app_macos/browser.app"
if [[ ! -e "$browser_app" ]]; then

    # Check if the use prefers a different browser
    browser_name=$(sed -e 's/#.*$//' -e '/^[ \t]*$/d' ~/.config/my-browser/conf || echo 'Google Chrome')

    # Check if the browser is already installed, if it is we copy it
    if [[ -e "/Applications/$browser_name.app" ]]; then
        cp_a "/Applications/$browser_name.app" "$browser_app"
    else
        # Download the browser
        download_browser "$browser_name" "$browser_app"
    fi
fi

# Start the browser
browser_exec "$browser_app" "$HOME/Library/Application Support/MyBrowser/$app_name" "$@"
